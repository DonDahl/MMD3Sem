<?php
    /*
     * Opgave 03_01
     * 
     * En event er en begivendhed der er afgr�nset ved tid og sted, og som er kendetegnet ved specifikt indhold.
     * 
     * Opgave 1) 
     * Find nogle flere events. S�g inspiration p� http://www.visitskive.dk/skive/begivenheder-2
     * Inds�t de fundne events i $events arrayet nede i klassen Event. Find latitude og longitude p� https://itouchmap.com/latlong.html
     *
     * Opgave 2)
     * I klassen Event er der en tom metode der hedder getAllEvents. Metoden skal returnere hele $events arrayet fra klassen.
     * N�r vi returnerer hele arrayet, s� kan vi anvende det andre steder. Fx i index.php
     *
     * Opgave 3)
     * G� til index.php
     * Lokaliser der hvor mark�rer bliver indsat p� kortet. Erstat de to eksisterende mark�rer med php kode. Koden skal l�be igennem det array, som metoden getAllEvents 
     * returnerer og udskrive indholdet, s�ledes, at hver gang l�kken l�ber en gang, vises en mark�r p� kortet.
     * 
     * 
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ),
		array(
            "EventId"=>4,
            "EventName"=>"Limfjorden Rundt Tr�skibssejlads",
            "EventDescription"=>"Limfjorden Rundt",
            "EventDate"=>"September 13 - 17 2016",
            "Lat"=>"56.57",
            "Long"=>"9.0",
            "EventImage"=>"img/limfjordenRundt.png"
        ),array(
            "EventId"=>5,
            "EventName"=>"Oyster Trophy Week 2016",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 11 - 20 2016",
            "Lat"=>"56.6",
            "Long"=>"9.04",
            "EventImage"=>"img/oyster.png"
        ));
        function __construct()
        {
            //Konstrukt�r (funktionen) skal ikke benyttes
        }
        function getAllEvents()
        {
            foreach($this->events as $ev){
				
				$keys = array_keys($ev);
				echo 'L.marker([' . $ev["Lat"] . ',' .$ev["Long"] . ']).addTo(mymap).bindPopup("<b>' . $ev["EventName"] . '</b><br />' . $ev["EventDate"] . '");';
			}
        }
		
    }
		
?>