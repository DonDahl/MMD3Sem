<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden removePersonFromArray skal kunne fjerne en person fra et indexeret array.
     * Brug den indbyggede metode array_slice().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Slicing Array
     */
    
    class Person
    {
        function removePersonFromArray()
        {
            $people = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
			$order = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
			list($second, $third, $fourth, $fifth) = array_slice($order, 1, 4);
			echo "$second, $third, $fourth, $fifth";
			//$middle = array_slice($people, 2, 2); // $middle is array("Harriet", "Brenda")
			//echo implode(" ",$middle);
        }
    }
    $person = new Person;
    $person->removePersonFromArray();
?>