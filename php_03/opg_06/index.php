<?php
    /*
     * Opgave 03_06
     * 
     * Metoden getEventById skal kunne lokalisere en event ud fra id.
     * N�r metoden har fundet det array der indeholder den specifikke instans, skal metoden returnere dette array.
     * HJ�LP: Med en foreach l�kke, kan du l�be arrayet igennem. For hvert genneml�b kan du evaluere, om der en en n�gle med en bestemt v�rdi.
     * Se kapitel - Array -> Traversing Arrays
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ));
        function __construct()
        {
        }
        function getEventById()
        {
			foreach($this->events as $ev){
				$keys = array_keys($ev);
				echo "$ev[EventId]<br>";
				echo "$ev[EventName]<br>";
				echo "$ev[EventDescription]<br>";
				echo "$ev[EventDate]<br>";
				echo "$ev[Lat]<br>";
				echo "$ev[Long]<br>";
				echo "$ev[EventImage]<br><br>";
			}
        }
    }
		$event = new Event;
		$event->getEventById();
?>