<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<?php
$countries = array(
'Italy'=>'Rome',
'Denmark'=>'Copenhagen',
'Belgium'=>'Brussels',
'Finland'=>'Helsinki',
'Germany'=>'Berlin',
'Greece'=>'Athens',
'Ireland'=>'Dublin',
'Netherlands'=>'Amsterdam',
'Spain'=>'Madrid',
'Sweden'=>'Stockholm',
'United Kingdom'=>'London',
'Czech Republic'=>'Prague',
'Estonia'=>'Tallin',
'Hungary'=>'Budapest',
'Latvia'=>'Riga',
'Poland'=>'Warsaw',
'Austria'=>'Vienna',
'Malta'=>'Valetta',
'Slovenia'=>'Ljubljana',
'Slovakia'=>'Bratislava'
);
?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Not sorted</h2>
				<?php
				foreach($countries as $country => $capital)
				echo "$country - $capital<br>";
				?>
            </div>
            <div class="col-md-3">
                <h2>Sort Country</h2>
				<?php
				ksort($countries);
				foreach($countries as $country => $capital){
					echo "$country - $capital<br>";
				}
				?>
            </div>
            <div class="col-md-3">
                <h2>Sort Capital</h2>
				<?php
				asort($countries);
				foreach($countries as $country => $capital){
					echo "$capital - $country<br>";
				}
				?>
            </div>
        </div>
    </div>
<body>