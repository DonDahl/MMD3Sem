<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     //Simpel streng
     $email = "kl@eadania.dk";

     //Simpel int.
     $phone = 41770410;
     
     //Et simpelt array, der kan tilgås ved hjælp af index.
     $preferences = array('Programmering','Organisation og udvikling','Undervisning og læring');

     //Et associativt array, der består af en key og en value.
     $jobs = array('Fiat Herning' => 'Hjemmeside','Scholl sko' => 'Hjemmeside og webshop','Grameta' => 'Hjemmeside og webshop','N Graversen' => 'Hjemmeside og digitale strategier');

     //Et kompliceret array, der består af simple elementer og simple array. 
     $competencies = array('Undervisning',array('PHP','C#','HTML','CSS'), array('AngularJS','Bootstrap','Foundation'));

?>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
				<?php
				echo $email .'<br>'. $phone;
				?>
            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>
				<?php
				foreach ($preferences as $preference) {
					echo "$preference<br>";
				}
				?>
            </div>
            <div class="col-md-3">
                <h2>Portefolje!</h2>
				<?php
				foreach ($jobs as $workplace => $job ){
					echo "$workplace - $job<br>";
				}
				?>
            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>
				<?php
				foreach ($competencies as $comp) {
                            if(is_array($comp)){
                                foreach ($comp as $c) {
                                    echo "<li>" . $c . "</li>";
                                }
                            }else {
                                echo "<li>" . $comp . "</li>";
                            }
                        }
				?>
            </div>
        </div>
    </div>
<body>