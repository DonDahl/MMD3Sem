<?php

    /*
    *   En denne fil er placeret en r�kke partielt udfyldte funktioner, som har til form�l, at lave specifikke beregner p� pris og moms.
    *   Disse funktioner er typisk anvendelige i en webbutik.
    */

    //Funktion der udregner momsen p� en vare. Funktionen l�gger dansk moms til prisen og returnerer prisen inklusiv moms.
    //Fx koster en vare ex moms 80 kroner. Momsen er 25% af varens pris og derfor er prisen inklusiv moms 100kr.
    function returnPriceInclusiveVAT($price)
    {
       return $price*1.25;
    }

    //Funktion der udregner momsen p� en vare og returnere momsen.
    //fx koster en vare 100 kroner. 20% af prisen er moms og derfor er momsen p� varen 20 kroner.
    function returnDeductedVAT($price)
    {
		return $price*0.2;
    }

    //Funktion der udregner prisen p� varen eksklusiv moms og returnere denne.
    //fx er varens pris 100 kr inklusiv moms. Momsen er 20% af prisen og derfor returneres 80 kr.
    function returnPriceExcludingVAT($price)
    {
        return $price*0.8;
    }

    //Funktion der udregner prisen inklusiv moms, n�r kun moms er opgivet.
    //fx udg�r momsen 20 kr. Derfor m� den samlede pris inklusiv moms v�re 100 kroner. 
    function returnPriceFromVAT($vat)
    {
		return $vat *5;
    } 

    //Funktion der returnerer prisen p� en vare hvor rabatten er fratrukket
    //fx er prisen 80 kr ex moms. Rabbatten er 50% og derfor returneres 40 kr.
    function returnPriceWithDiscount($price, $discount)
    {
        /*$noVat = $price*0.8;
		$discount = 0.5;
        $price = $noVat*$discount;
		return $price;*/
		
		return $price-(($price*$discount)/100);
    }

    

?>